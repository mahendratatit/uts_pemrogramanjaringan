package com.example.uts_tokosepatu

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SimpleAdapter
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(){

    val COLLECTION = "katalog"
    val F_ID = "id"
    val F_MERK = "merk"
    val F_TIPE = "tipe"
    val F_DESKRIPSI = "deskripsi"
    val F_UKURAN = "UKURAN"
    var docId = ""
    var fbAuth = FirebaseAuth.getInstance()
    lateinit var db : FirebaseFirestore
    lateinit var alKatalog : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        alKatalog = ArrayList()
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            showData()
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alKatalog.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_MERK,doc.get(F_MERK).toString())
                hm.set(F_TIPE,doc.get(F_TIPE).toString())
                hm.set(F_DESKRIPSI,doc.get(F_DESKRIPSI).toString())
                hm.set(F_UKURAN,doc.get(F_UKURAN).toString())
                alKatalog.add(hm)
            }
            adapter = SimpleAdapter(this,alKatalog,R.layout.row_katalog,
                arrayOf(F_MERK,F_TIPE,F_DESKRIPSI,F_UKURAN),
                intArrayOf(R.id.txMerk, R.id.txTipe, R.id.txUkuran, R.id.txDeskripsi))
            lsView.adapter = adapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemAbout ->{
                var intent = Intent(this,AboutActivity::class.java)
                startActivity(intent)
            }
            R.id.itemLogout ->{
                fbAuth.signOut()
                val intent = Intent (this,MainActivity::class.java)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }
}